terraform {
  source = "git::git@gitdev.clarobrasil.mobi:devops/riverpipe/terraform/aws/modules/amazon-rds.git"

}

include {
  path = find_in_parent_folders()
}

inputs = {
  env=
}
