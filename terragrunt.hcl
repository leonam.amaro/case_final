generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
provider "aws" {
  region = "us-east-1"
}
EOF
}
locals{
   //env_var      = read_terragrunt_config(find_in_parent_folders("tags.hcl"))

}

remote_state {
  backend = "s3"
  config = {
    encrypt        = true
    bucket         = "terraform_state"
    key            = "${path_relative_to_include()}/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraform-locks"
  }
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }
}
