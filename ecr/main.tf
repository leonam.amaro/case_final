provider "aws" {
   region = "us-east-1"
}

resource "aws_ecr_repository" "ecr_leon_elo" {
  name                 = "case_final"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

terraform {
    backend "s3" {
        bucket = "leon-elo-case-final"
        region = "us-east-1"
        encrypt = "true"
        key = "project-name/terraform.tfstate"
    }
}