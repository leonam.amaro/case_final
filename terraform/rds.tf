resource "aws_db_instance" "case_final" {
  allocated_storage    = 10
  db_name              = "case_final_leon_elo"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t3.micro"
  username             = "casefinal"
  password             = "casefinal"
  parameter_group_name = "default.mysql5.7"
  skip_final_snapshot  = true
}