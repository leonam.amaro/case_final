resource "aws_lb" "test" {
  name               = "case-final"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [
    aws_security_group.node_group_one.id
    //aws_security_group.node_group_two.id
    ]
  subnets            = [
    for private_subnets in module.vpc.private_subnets : private_subnets
    ]

  enable_deletion_protection = false

  tags = {
    Environment = "Tracking"
  }
}