provider "aws" {
  region = "us-east-1"
}

locals {
  cluster_name="case_final1"
}

data "aws_availability_zones" "available" {}