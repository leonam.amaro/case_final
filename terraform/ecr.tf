resource "aws_ecr_repository" "ecr_leon_elo" {
  name                 = "case_final"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}