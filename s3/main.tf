provider "aws" {
  region = "us-east-1"
}


resource "aws_s3_bucket" "case_final" {
  bucket = "leon-elo-case-final"

  tags = {
    Name        = "Case_final"
    Environment = "Tracking"
  }
}

resource "aws_s3_bucket_acl" "example" {
  bucket = aws_s3_bucket.case_final.id
  acl    = "private"
}